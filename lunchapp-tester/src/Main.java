import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

public class Main {
	
	static String urlBasePath = "http://localhost:8080/";
	
	enum HttpMethods {
		GET,
		HEAD,
		POST,
		PUT,
		DELETE,
		CONNECT,
		OPTIONS,
		TRACE,
		PATCH
	}
	
	static class JsonHelper {
		private StringBuilder jsonString = new StringBuilder("{}");
		
		JsonHelper(StringBuilder jsonString) {
			this.jsonString = jsonString;
		}
		
		JsonHelper(String jsonString) {
			this.jsonString = new StringBuilder("{" + jsonString + "}");
		}
		
		JsonHelper() {}

		Boolean isEmptyJson() {
			if(this.getJsonToString().equals("{}")) {
				return true;
			}
			return false;
		}
		
		String getJsonToString() {
			return jsonString.toString();
		}
		
		void addKeyValuePair(String key, String value) {
			if(!this.getJsonToString().replaceAll("\\{|}", "").equals("")) {
				this.addComma();
			}
			if(value == null) {
				jsonString.insert(jsonString.length() - 1, '"' + key + '"' + ":" + value);
			} else {
				jsonString.insert(jsonString.length() - 1, '"' + key + '"' + ":" + '"' + value + '"');
			}
		}
		
		void addComma() {
			jsonString.insert(jsonString.length() - 1, ",");
		}
		
		String[] parseJsonToStringArray(String jsonToBeParsed) {
			return jsonToBeParsed.replaceAll("\\{|}|\"|\\s|", "").split(",");
		}
		
		String getValueByKey(String keyToFind) {
			String[] extractedKeysAndValues = parseJsonToStringArray(this.getJsonToString());
			
			for(String temporaryKeyValuePair : extractedKeysAndValues) {
				if(temporaryKeyValuePair.substring(0, temporaryKeyValuePair.indexOf(':')).equals(keyToFind)) {
					return temporaryKeyValuePair.substring(temporaryKeyValuePair.indexOf(':') + 1, temporaryKeyValuePair.length());
				}
			}
			
			return "";
		}
		
		boolean equals(JsonHelper jsonString) {
			try {
				if(this == jsonString) {
					return true;
				}
				
				String[] extractedKeysAndValuesLocal = parseJsonToStringArray(this.getJsonToString());
				String[] extractedKeysAndValuesInputed = parseJsonToStringArray(jsonString.getJsonToString());
				
				for(int i = 0; i < extractedKeysAndValuesLocal.length; i++) {
					if(!Arrays.asList(extractedKeysAndValuesLocal).contains(extractedKeysAndValuesInputed[i])) {
						return false;
					}
				}
				
				return true;
			} catch(ArrayIndexOutOfBoundsException e) {
				return false;
			}

		}
		
	}
	
	static class HttpRequestResult {
		private int responseCode;
		private JsonHelper responseBody;
		
		HttpRequestResult(int responseCode, JsonHelper responseBody) {
			this.responseCode = responseCode;
			this.responseBody = responseBody;
		}
		
		HttpRequestResult(int responseCode, String responseBody) {
			this.responseCode = responseCode;
			this.responseBody = new JsonHelper(responseBody);
		}
		
		HttpRequestResult(int responseCode) {
			this.responseCode = responseCode;
			this.responseBody = new JsonHelper();
		}
		
		int getReturnedResponseCode() {
			return responseCode;
		}
		
		JsonHelper getReturnedResponseBody() {
			return responseBody;
		}
		
		String getReturnedResponseBodyToString() {
			return responseBody.getJsonToString();
		}
		
		boolean equalsResponseCode(HttpRequestResult httpRequestResult) {
			if(this.getReturnedResponseCode() == httpRequestResult.getReturnedResponseCode()) {
				return true;
			}
				
			return false;
		}
		
		boolean equals(HttpRequestResult httpRequestResult) {
			if(this == httpRequestResult) {
				return true;
			}
			
			if(this.getReturnedResponseCode() == httpRequestResult.getReturnedResponseCode() && this.getReturnedResponseBody().equals(httpRequestResult.getReturnedResponseBody())) {
				return true;
			}
				
			return false;
		}
	}
	
	static HttpURLConnection openHTTPConnection(String requestTargetURL, HttpMethods requestMethod) throws IOException {
		URL urlForRequest = new URL(requestTargetURL);
		HttpURLConnection httpConnection = (HttpURLConnection) urlForRequest.openConnection();
		httpConnection.setRequestMethod(requestMethod.toString());
		
		return httpConnection;
	}
	
	static void setHeadersForJson(HttpURLConnection httpConnection) {
		httpConnection.setRequestProperty("Content-Type", "application/json; utf-8");
		httpConnection.setRequestProperty("Accept", "application/json");
		httpConnection.setDoOutput(true);
	}
	
	static void writeToOutputStream(HttpURLConnection httpConnection, JsonHelper requestBody) throws IOException {
		OutputStream outputStream = httpConnection.getOutputStream();
		
		outputStream.write(requestBody.getJsonToString().getBytes());
		outputStream.flush();
		outputStream.close();
	}
	
	static StringBuilder retrieveResponseBody(HttpURLConnection httpConnection) throws IOException {
		BufferedReader inputStream = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
		StringBuilder response = new StringBuilder();
		String inputLine;
		
		while((inputLine = inputStream.readLine()) != null) {
			response.append(inputLine);
		}
		inputStream.close();
		
		return response;
	}
	
	static HttpRequestResult processHttpRequest(String requestTargetURL, HttpMethods requestMethod) throws IOException {
		HttpURLConnection httpConnection = openHTTPConnection(requestTargetURL, requestMethod);
		
		return new HttpRequestResult(httpConnection.getResponseCode(), new JsonHelper(retrieveResponseBody(httpConnection)));
	}
	
	// Overload processHTTPRequest function in case of a request that requires a body
	static HttpRequestResult processHttpRequest(String requestTargetURL, HttpMethods requestMethod, JsonHelper requestBody) throws IOException {
		HttpURLConnection httpConnection = openHTTPConnection(requestTargetURL, requestMethod);
		setHeadersForJson(httpConnection);
		writeToOutputStream(httpConnection, requestBody);

		return new HttpRequestResult(httpConnection.getResponseCode(), new JsonHelper(retrieveResponseBody(httpConnection)));
	}
	
	static Boolean testUserGetByUuid() {
		Boolean resultPassing = false;
		Boolean resultFailing = false;
		
		try {
			JsonHelper testJson = new JsonHelper();
			testJson.addKeyValuePair("uuid", "ff8081816e849796016e855a74dc000c");
			testJson.addKeyValuePair("firstName", "Martin");
			testJson.addKeyValuePair("lastName", "Todorov");
			testJson.addKeyValuePair("sessionToken", "BQVfq3HW26bjhP18OqR3FV-dTs72NxDC");
			testJson.addKeyValuePair("email", "martintodorov@gmail.com");
			testJson.addKeyValuePair("password", "$2a$10$gAU.J28QhCaaDhAn058MRe1vHlix4QQ2XlpNeySVD0EziE.c2zeNW");
			testJson.addKeyValuePair("meetingConnections", "[]");		
			
			resultPassing = processHttpRequest(urlBasePath + "user/ff8081816e849796016e855a74dc000c", HttpMethods.GET).equals(new HttpRequestResult(200, testJson));
			resultFailing = processHttpRequest(urlBasePath + "user/invalidUuid", HttpMethods.GET).equals(new HttpRequestResult(200));
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(resultPassing && resultFailing) {
				return true;
			}
		}
		
		return false;
	}
	
	static Boolean testUserRegister() {
		Boolean resultPassing = false;
		Boolean resultFailing = false;
		
		try {
			JsonHelper requestBody = new JsonHelper();
			requestBody.addKeyValuePair("firstName", "Martin");
			requestBody.addKeyValuePair("lastName", "Todorov");
			requestBody.addKeyValuePair("email", "martintodorov@gmail.com");
			requestBody.addKeyValuePair("password", "123456");
						
			JsonHelper expectedResultBody = new JsonHelper();
			expectedResultBody.addKeyValuePair("message", "Register successfull!");
			
			resultPassing = processHttpRequest(urlBasePath + "user/register", HttpMethods.POST, requestBody).equals(new HttpRequestResult(200, expectedResultBody));
			resultFailing = processHttpRequest(urlBasePath + "user/register", HttpMethods.POST, requestBody).equalsResponseCode(new HttpRequestResult(500));
		} catch(IOException e) {
			resultFailing = true;
		} finally {			
			if(resultPassing && resultFailing) {
				return true;
			}
		}
		
		return false;
	}
	
	static Boolean testUserLogin() {
		Boolean resultPassing = false;
		Boolean resultFailing = false;
		
		try {
			JsonHelper requestBody = new JsonHelper();
			requestBody.addKeyValuePair("email", "martintodorov_@gmail.com");
			requestBody.addKeyValuePair("password", "123456");
			
			JsonHelper requestBodyWrongInput = new JsonHelper();
			requestBodyWrongInput.addKeyValuePair("email", "martintodorov_@gmail.com");
			requestBodyWrongInput.addKeyValuePair("password", "wrongPassword");
			
			resultPassing = processHttpRequest(urlBasePath + "user/login", HttpMethods.POST, requestBody).equalsResponseCode(new HttpRequestResult(200));
			resultFailing = processHttpRequest(urlBasePath + "user/login", HttpMethods.POST, requestBodyWrongInput).equalsResponseCode(new HttpRequestResult(500));			
		} catch(IOException e) {
			resultFailing = true;
		} finally {
			if(resultPassing && resultFailing) {
				return true;
			}
		}
		
		return false;
	}
	
	static Boolean testUserGetMeetings() {
		Boolean resultPassing = false;
		Boolean resultFailing = false;
		
		try {
			JsonHelper requestBody = new JsonHelper();
			requestBody.addKeyValuePair("sessionToken", "BQVfq3HW26bjhP18OqR3FV-dTs72NxDC");
			
			JsonHelper requestBodyEmptyInput = new JsonHelper();
						
			resultPassing = processHttpRequest(urlBasePath + "user/meetings", HttpMethods.POST, requestBody).equals(new HttpRequestResult(200, ""));
			resultFailing = processHttpRequest(urlBasePath + "user/meetings", HttpMethods.GET, requestBodyEmptyInput).equalsResponseCode(new HttpRequestResult(500));
		} catch(IOException e) {
			resultFailing = true;
		} finally {
			if(resultPassing && resultFailing) {
				return true;
			}
		}
		
		return false;
	}
	
	static Boolean testMeetingCreate() {
		Boolean resultPassing = false;
		Boolean resultFailing = false;
		
		try {
			JsonHelper requestBody = new JsonHelper();
			requestBody.addKeyValuePair("sessionToken", "_i0S7-OpQsYDo9uUHO3rxKZZi_E6OD_5");
			
			JsonHelper requestBodyEmptyInput = new JsonHelper();
			
			resultPassing = processHttpRequest(urlBasePath + "meeting/ff8081816e8e4f19016e9004539d004b/create", HttpMethods.POST, requestBody).equalsResponseCode(new HttpRequestResult(200));
			resultFailing = processHttpRequest(urlBasePath + "meeting/ff8081816e8e4f19016e9004539d004b/create", HttpMethods.POST, requestBodyEmptyInput).equalsResponseCode(new HttpRequestResult(500));
		} catch(IOException e) {
			resultFailing = true;
		} finally {			
			if(resultPassing && resultFailing) {
				return true;
			}
		}
		
		return false;
	}
	
	static Boolean testMeetingGetByUuid() {
		Boolean resultPassing = false;
		Boolean resultFailing = false;
		
		try {				
			resultPassing = processHttpRequest(urlBasePath + "meeting/ff8081816e8e4f19016e9286b6bd0091", HttpMethods.GET).equalsResponseCode(new HttpRequestResult(200));
			resultFailing = processHttpRequest(urlBasePath + "meeting/invalidMeetingUuid", HttpMethods.GET).equals(new HttpRequestResult(200, ""));
		} catch(IOException e) {
			resultFailing = true;
		} finally {			
			if(resultPassing && resultFailing) {
				return true;
			}
		}
		
		return false;
	}
	
	static Boolean testMeetingGetInviteToken() {
		Boolean resultPassing = false;
		Boolean resultFailing = false;
		
		JsonHelper requestBody = new JsonHelper();
		requestBody.addKeyValuePair("inviteToken", "HvNE1TOfU5oMGmLT7Mp2_-hGMoxXHyIu");
		
		JsonHelper requestBodyWrongInput = new JsonHelper();
		requestBodyWrongInput.addKeyValuePair("inviteToken", "null");
		
		try {				
			resultPassing = processHttpRequest(urlBasePath + "meeting/ff8081816e8e4f19016e9286b6bd0091/getInviteToken", HttpMethods.GET).equals(new HttpRequestResult(200, requestBody));
			resultFailing = processHttpRequest(urlBasePath + "meeting/invalidMeetingUuid/getInviteToken", HttpMethods.GET).equals(new HttpRequestResult(200, requestBodyWrongInput));
		} catch(IOException e) {
			resultFailing = true;
		} finally {			
			if(resultPassing && resultFailing) {
				return true;
			}
		}
		
		return false;
	}
	
	static Boolean testMeetingJoin() {
		Boolean resultPassing = false;
		Boolean resultFailing = false;
		
		JsonHelper requestBody = new JsonHelper();
		requestBody.addKeyValuePair("sessionToken", "YDKce7LLuVbiiGAuptfdchmdYEGCRgr2");
		
		JsonHelper expectedResponse = new JsonHelper();
		expectedResponse.addKeyValuePair("message", "You have joined the meeting successfully");
		
		JsonHelper requestBodyWrongInput = new JsonHelper();
		
		try {				
			resultPassing = processHttpRequest(urlBasePath + "meeting/join/HvNE1TOfU5oMGmLT7Mp2_-hGMoxXHyIu", HttpMethods.POST, requestBody).equals(new HttpRequestResult(200, expectedResponse));
			resultFailing = processHttpRequest(urlBasePath + "meeting/join/invalidInviteToken", HttpMethods.POST, requestBodyWrongInput).equalsResponseCode(new HttpRequestResult(500));
		} catch(IOException e) {
			resultFailing = true;
		} finally {			
			if(resultPassing && resultFailing) {
				return true;
			}
		}
		
		return false;
	}
	
	public static void main(String[] args) throws IOException {
		System.out.println("USER GET BY UUID: " + testUserGetByUuid());
		System.out.println("USER REGISTER: " + testUserRegister());
		System.out.println("USER LOGIN: " + testUserLogin());
//		System.out.println("USER GET MEETINGS: " + testUserGetMeetings());
		System.out.println("MEETING CREATE: " + testMeetingCreate());
		System.out.println("MEETING GET BY UUID: " + testMeetingGetByUuid());
		System.out.println("MEETING JOIN: " + testMeetingJoin());
		System.out.println("MEETING GET INVITE TOKEN: " + testMeetingGetInviteToken());
	}
}
