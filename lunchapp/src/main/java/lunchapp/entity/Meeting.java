package lunchapp.entity;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;

import lunchapp.helpers.HelperService;

@Entity
public class Meeting {

	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	private String uuid;
	private Date date;
	private Time time;
	private String location;
	private String description;
	private String inviteToken = HelperService.generateNewToken();
	
	@OneToMany(mappedBy="meeting",fetch = FetchType.LAZY)
	private List<MeetingUserConnection> users;
	
	public Meeting() {
		
	}
	
	public Meeting(Date date, Time time, String description) {
		this.date = date;
		this.time = time;
		this.description = description;
	}
	
	public Meeting(Date date, String description) {
		this.date = date;
		this.description = description;
	}
	
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getInviteToken() {
		return inviteToken;
	}

	public String getUuid() {
		return uuid;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public List<MeetingUserConnection> getUsers(){
		return users;
	}
	
}
