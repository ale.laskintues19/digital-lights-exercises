package lunchapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class MeetingUserConnection {
	
	@Id 
	@GeneratedValue
	@Column(unique = true)
	private long id;
	
	private boolean isOwner;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user")
	private User user;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "meeting")
	private Meeting meeting;
	
	public MeetingUserConnection() {

	}
	
	public MeetingUserConnection(Meeting meeting, User user, boolean isOwner) {
		this.user = user;
		this.meeting = meeting;
		this.isOwner = isOwner;
	}
	public boolean isOwner() {
		return isOwner;
	}

	public void setOwner(boolean isOwner) {
		this.isOwner = isOwner;
	}

	public long getId() {
		return id;
	}

}
