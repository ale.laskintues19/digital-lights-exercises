package lunchapp.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;


@Entity
public class User {


	@Id
	@Column(unique = true)
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid",strategy = "uuid")
	private String uuid;
	
	@NotNull
	private String firstName;
	
	@NotNull
	private String lastName;
	
	private String sessionToken;

	@Column(unique = true)
    @NotNull
	@Email
	private String email;
	
	@NotNull
	private String password;
	
	@OneToMany(mappedBy="user",fetch = FetchType.LAZY)
	private List<MeetingUserConnection> meetingConnections;

	public User() {}
	
	
	public User(String firstName, String lastName, String email, String password) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}
	
	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getSessionToken() {
		return sessionToken;
	}
	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public List<MeetingUserConnection> getMeetingConnections(){
		return meetingConnections;
	}

	public String getUuid() {
		return uuid;
	}

	
}