package lunchapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import lunchapp.entity.Meeting;
import lunchapp.entity.MeetingUserConnection;
import lunchapp.entity.User;

public interface MeetingUserConnectionRepository extends PagingAndSortingRepository<MeetingUserConnection, Long> {

	@Query("SELECT muc.user FROM MeetingUserConnection muc WHERE muc.meeting = ?1")
	List<User> getUsersOfMeeting(Meeting meeting);
	
	@Query("SELECT muc.meeting FROM MeetingUserConnection muc WHERE muc.user = ?1")
	List<Meeting> getMeetingsOfUser(User user);
}
