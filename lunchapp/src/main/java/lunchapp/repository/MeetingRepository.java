package lunchapp.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import lunchapp.entity.Meeting;

@Repository
@Transactional
public interface MeetingRepository extends PagingAndSortingRepository<Meeting, String> {

	@Query("SELECT m FROM Meeting m WHERE m.uuid = ?1")
	Meeting findByUuid(String uuid);
	
	@Query("SELECT m.inviteToken FROM Meeting m WHERE m.uuid = ?1")
	String getInviteTokenOfMeeting(String uuid);
	
	@Query("SELECT m FROM Meeting m WHERE m.inviteToken = ?1")
	Meeting findByInviteToken(String inviteToken);

}
