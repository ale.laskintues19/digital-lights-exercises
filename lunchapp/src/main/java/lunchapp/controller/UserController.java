package lunchapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;

import lunchapp.entity.Meeting;
import lunchapp.entity.User;
import lunchapp.exceptions.UserAuthenticationException;
import lunchapp.helpers.HelperService;
import lunchapp.repository.MeetingUserConnectionRepository;
import lunchapp.repository.UserRepository;

@RestController
@RequestMapping("/user")
public class UserController {
	//TODO add automated getting of meetings of user
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private MeetingUserConnectionRepository meetingUserConnectionRepository;
	
	private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	
	@PostMapping("/register")
	private String register(@RequestBody User newUser) {
		newUser.setPassword(encoder.encode(newUser.getPassword()));
		userRepository.save(newUser);
		return HelperService.toJson("message","Register successfull!");
	}
	
	@GetMapping("/{uuid}")
	private User user(@PathVariable String uuid) {
		User user = userRepository.findByUuid(uuid);
		return user;
	}
	
	@PostMapping("/login")
	private String loginAndReturnSessionToken(@RequestBody ObjectNode emailAndPasswordInJson) {
		String email = emailAndPasswordInJson.get("email").asText();
		String password = emailAndPasswordInJson.get("password").asText();
		User user = authenticateAndReturnUser(email, password);
		String newSessionToken = HelperService.generateNewToken();
		user.setSessionToken(newSessionToken);
		userRepository.save(user);
		return HelperService.toJson("sessionToken",newSessionToken);
	}
	
	@GetMapping("/meetings")
	private List<Meeting> getAllMeetingsOfAUser(@RequestBody String sessionTokenJson){
		User user = getUserBySessionTokenInJson(userRepository,sessionTokenJson);
		List<Meeting> meetings = meetingUserConnectionRepository.getMeetingsOfUser(user);
		return meetings;
		
	}
	
	private User authenticateAndReturnUser(String email, String password) {
		User user = userRepository.findByEmail(email);
		if(encoder.matches(password, user.getPassword())) {
			return user;
		}else {
			throw new UserAuthenticationException("Wrong email or password !");
		}
	}
	
	public static User getUserBySessionTokenInJson(UserRepository userRepository,String jsonBody) {
		String sessionToken = HelperService.valueOfARepresentingKeyInJsonString("sessionToken",jsonBody);
		return userRepository.findBySessionToken(sessionToken);
	}
	
}


