package lunchapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lunchapp.entity.Meeting;
import lunchapp.entity.MeetingUserConnection;
import lunchapp.entity.User;
import lunchapp.helpers.HelperService;
import lunchapp.repository.MeetingRepository;
import lunchapp.repository.MeetingUserConnectionRepository;
import lunchapp.repository.UserRepository;

@RestController
@RequestMapping("/meeting")
public class MeetingController {
	//TODO add  generation of meeting with random users
	@Autowired
	private MeetingRepository meetingRepository;
	
	@Autowired
	private MeetingUserConnectionRepository meetingUserConnectionRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/{uuid}")
	private Meeting getMeetingByUuid(@PathVariable String uuid) {
		Meeting meeting = meetingRepository.findByUuid(uuid);
		return meeting;
	}
	
	@PostMapping("/{uuid}/create")
	private String createMeetingAndReturnItsUuid(@RequestBody String sessionTokenJson) {
		User user = UserController.getUserBySessionTokenInJson(userRepository, sessionTokenJson);
		Meeting meeting = new Meeting();
		meetingRepository.save(meeting);
		meetingUserConnectionRepository.save(new MeetingUserConnection(meeting,user,true));

		return HelperService.toJson("meetingUuid",meeting.getUuid());
	}
	@PostMapping("/join/{inviteToken}")
	private String joinByInviteToken(@PathVariable String inviteToken, @RequestBody String jsonBody) {
		User invited = UserController.getUserBySessionTokenInJson(userRepository,jsonBody);
		Meeting invitedTo = meetingRepository.findByInviteToken(inviteToken);
		joinMeeting(invited,invitedTo);
		return HelperService.toJson("message", "You have joined the meeting successfully");
	}
	
	@GetMapping("/{uuid}/getInviteToken")
	private String getInviteToken(@PathVariable String uuid) {
		return HelperService.toJson("inviteToken",meetingRepository.getInviteTokenOfMeeting(uuid));
	}
	
	@GetMapping("/{uuid}/getUsers")
	private List<User> getAttenders(@PathVariable String uuid) {
		Meeting meeting = meetingRepository.findByUuid(uuid);
		List<User> attenders = meetingUserConnectionRepository.getUsersOfMeeting(meeting);
		
		return attenders;
	}
	
	private void joinMeeting(User invited, Meeting invitedTo) {
		meetingUserConnectionRepository.save(new MeetingUserConnection(invitedTo,invited,false));

	}
	

}
